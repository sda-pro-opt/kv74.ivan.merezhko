#include "LexicalAnalyzer.h"
#include <filesystem>
namespace fs = std::filesystem;

void TestLexicalAnalyzer()
{
    fs::path dir = "C:\\Users\\Ivan\\Desktop\\KPI\\3-curs\\6-semestr\\Translator\\Translator\\TestData";
    if (!fs::exists(dir))
    {
        std::cout << "UUPPS";
        return;
    }
    LexicalAnalyzer la;
    la.StartAnalyze("C:\\Users\\Ivan\\Desktop\\KPI\\3-curs\\6-semestr\\TranslatorVS\\TranslatorVS\\TestData\\1.txt");
    la.PrintResult();
    const auto& errors = la.GetErrors();
    for (const auto& error : errors)
    {
        std::cout << error << '\n';
    }
}

int main()
{
    TestLexicalAnalyzer();
	std::cin.get();
    return 0;
}