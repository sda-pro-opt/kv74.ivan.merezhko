#include "../lab1/LexicalAnalyzer.h"
#include "../lab2/SyntacticalAnalyzer.h"
#include <filesystem>
#include <iostream>
#include <fstream>
namespace fs = std::filesystem;

void TestLexicalAnalyzer()
{
    fs::path dir = "X:\\KPI\\6-semestr\\TranslatorLab\\TranslatorLab";
    if (!fs::exists(dir))
    {
        std::cout << "UUPPS";
        return;
    }
    std::ofstream file("out.txt");
    LexicalAnalyzer la;
    la.StartAnalyze("X:\\KPI\\6-semestr\\TranslatorLab\\TranslatorLab\\testData.txt");
    la.PrintResult();
    if (const auto& errors = la.GetErrors();
        !errors.empty())
    {
        for (const auto& error : errors)
        {
            std::cout << error << '\n';
        }
        return;
    }
    SyntacticalAnalyzer sa(la.GetContext(), la.GetTokensInfoVector());
    sa.StartAnalyze(true);

    /*
    for (fs::directory_iterator it(dir), end; it != end; ++it)
    {
        if (it->path().extension() == ".txt")
        {
            std::cout << "\n======" << it->path() << "=======\n";

//            la.StartAnalyze(it->path().c_str());
            la.StartAnalyze("C:\\Users\\Ivan\\Desktop\\KPI\\3 curs\\6-semestr\\Translator\\Translator\\TestData\\1.txt");

            std::cout << "\n=============\n";
        }
    }
*/
}

int main()
{
    TestLexicalAnalyzer();
	std::cin.get();
    return 0;
}