#include "../lab1/LexicalAnalyzer.h"
#include "../lab2/SyntacticalAnalyzer.h"
#include "../lab3/CodeGenerator.h"
#include <filesystem>
#include <fstream>
#include <iostream>
namespace fs = std::filesystem;

bool compareFiles(const std::string& p1, const std::string& p2) {
    std::ifstream f1(p1, std::ifstream::binary | std::ifstream::ate);
    std::ifstream f2(p2, std::ifstream::binary | std::ifstream::ate);

    if (f1.fail() || f2.fail()) {
        return false;
    }

    if (f1.tellg() != f2.tellg()) {
        return false;
    }

    f1.seekg(0, std::ifstream::beg);
    f2.seekg(0, std::ifstream::beg);
    return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
        std::istreambuf_iterator<char>(),
        std::istreambuf_iterator<char>(f2.rdbuf()));
}

void TestLexicalAnalyzer()
{
    fs::path dir = "TestData";
    if (!fs::exists(dir))
    {
        std::cout << "TestData is not exists";
        return;
    }
    for (fs::directory_iterator it(dir), end; it != end; ++it)
    {
        LexicalAnalyzer la;
        const auto input = it->path().string() + "\\input.sig";
        const auto generate = it->path().string() + "\\generated.txt";
        const auto expected = it->path().string() + "\\expected.txt";
        la.StartAnalyze(input);
        SyntacticalAnalyzer sa(la.GetContext(), la.GetTokensInfoVector());
        sa.StartAnalyze(false);
        if (!sa.GetStatus())
        {
            continue;
        }
        std::ofstream generatedFile(generate);
        CodeGenerator generator(sa.GetTree());
        generator.Generate(std::move(generatedFile));
        if (compareFiles(expected, generate))
        {
            std::cout << "Test in " << it->path().string() << " PASSED\n";
        }
        else
        {
            std::cout << "Test in " << it->path().string() << " FAILED\n";
        }
    }
}

int main()
{
    TestLexicalAnalyzer();
    return 0;
}