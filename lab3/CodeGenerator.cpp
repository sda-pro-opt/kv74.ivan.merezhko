#include "CodeGenerator.h"
#include <iostream>
#include <string>


namespace
{
	std::string GenerateError(const TokenInfo& alreadyUsed, const TokenInfo& newToken)
	{
		const auto& alreadyUsedLine = std::get<2>(alreadyUsed);
		const auto& alreadyUsedColumn = std::get<3>(alreadyUsed);
		const auto& newLine = std::get<2>(newToken);
		const auto& newColumn = std::get<3>(newToken);
		std::string error;
		error.append("Line:")
			.append(std::to_string(newLine))
			.append(" Column:")
			.append(std::to_string(newColumn))
			.append(" Error: variable with this name: \"")
			.append(std::get<0>(alreadyUsed))
			.append("\" already defined at Line:")
			.append(std::to_string(alreadyUsedLine))
			.append(" Column:")
			.append(std::to_string(alreadyUsedColumn));
		return error;
	}
}

CodeGenerator::CodeGenerator(const Tree& tree)
	: m_tree(tree)
{}

void CodeGenerator::Generate(std::ofstream&& output)
{
	m_file = std::move(output);
	generate(m_tree.m_root);
	if (m_error)
	{
		m_file << *m_error;
	}
	m_file.close();
}

bool CodeGenerator::generate(const NodePtr& node)
{
	if (!node)
	{
		return true;
	}
	if (node->m_address)
	{
		switch (*node->m_address)
		{
		case SignalProgramE:
			writeProgram(node);
			break;
		case FunctionListE:
			if (!writeFunction(node->m_child))
			{
				return false;
			}
			break;
		case WriteE:
			writeBeginAndEnd(node);
			break;
		case EmptyStatementListE:
			m_file << "nop" << '\n';
			break;
		default:
			break;
		}
	}

	if (!generate(node->m_child))
	{
		return false;
	}
	if (!generate(node->m_next))
	{
		return false;
	}
	return true;
}

void CodeGenerator::writeProgram(const NodePtr& node)
{
	const auto& name = node->m_child->m_next->m_token;
	m_usedNames.push_back(name);
	m_file << "cute Program named " << std::get<0>(name) << "\n";
}
bool CodeGenerator::writeFunction(const NodePtr& node)
{
	const auto& name = node->m_token;
	if (const auto it = std::find_if(m_usedNames.begin(), m_usedNames.end(), [&](const auto& token)
		{
			return std::get<0>(token) == std::get<0>(name);
		});
		it != m_usedNames.end())
	{
		m_error = GenerateError(*it, name);
		return false;
	}
	else
	{
		m_usedNames.push_back(name);
	}
	const auto& firstValue = node->m_next->m_next;
	const auto& secondValue = node->m_next->m_next->m_next->m_child->m_next;
	const auto firstElement = std::stod(std::get<0>(firstValue->m_token)) / std::stod(std::get<0>(secondValue->m_token));
	const auto& thirdValue = node->m_next->m_next->m_next->m_child->m_next->m_next->m_next;
	m_file << "cute array named " << std::get<0>(node->m_token) << " " <<firstElement << " , " << std::get<0>(thirdValue->m_token) << '\n';
	return true;
}
void CodeGenerator::writeBeginAndEnd(const NodePtr& node)
{
	const auto& name = std::get<0>(node->m_token);
	if (name == "BEGIN" || name == "END")
	{
		m_file << "cute  " << name << "\n";
	}
}