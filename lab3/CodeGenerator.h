#include "../TreeElements/Tree.h"
#include <fstream>
class CodeGenerator
{
public:
	explicit CodeGenerator(const Tree& tree);
	void Generate(std::ofstream&& output);
private:
	bool generate(const NodePtr&);
	void writeProgram(const NodePtr&);
	bool writeFunction(const NodePtr&);
	void writeBeginAndEnd(const NodePtr&);
private:
	Tree m_tree;
	TokensInfoVector m_usedNames;
	std::optional<std::string> m_error;
	std::ofstream m_file;
};